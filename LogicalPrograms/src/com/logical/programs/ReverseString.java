package com.logical.programs;

import java.io.*;
import java.util.*;

public class ReverseString {
public static void main(String[] args) {
	Scanner s = new Scanner(System.in);
	System.out.println("Enter the String ");
	String s1= s.next();
	String s2 = "";
	char[] ch = s1.toCharArray();
	for(int i=ch.length-1;i>=0;i--) {
		s2=s2+s1.charAt(i);
	}
	System.out.println(s2);
}
}
